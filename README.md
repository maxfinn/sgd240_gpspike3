﻿# Spike Report

## Indoor Lighting

![Scene with light-cards][Lighting05]

### Introduction

Unreal Engine has a large number of different light types, and many ways to shadow objects, with very different effects on performance (and different available options for different platforms).

Lights are a very complex topic, and good-looking lighting and fast lighting are both things which we don’t know how to do (let alone good-looking and fast lighting)! 

To begin with, we will learn how to set up lighting in a controlled indoor environment.

### Goals

1. Tech: What types of Lighting is available in the Unreal Engine?
1. Skill: How do you make lighting look good?
1. Knowledge: What are the different Light Types available? 
1. Knowledge: What is a Lightmass, and how do you use the Lightmass Importance Volume?
1. Tech: How can Unreal bring lighting into an indoors scene “from outside”.
1. Skill: How to set up non-screen space reflections in Unreal Engine.
1. Skill: How to re-create a Three Point Lighting setup using Unreal Engine lights

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Lighting Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/Rendering/LightingAndShadows/QuickStart/index.html)
* [Simulating Area Lights in UE4](https://www.unrealengine.com/en-US/blog/simulating-area-lights-in-ue4?lang=en-US)
* [Lightmass Portals](https://docs.unrealengine.com/latest/INT/Support/Builds/ReleaseNotes/2016/4_11/index.html#new:lightmassportals)
* [Hanny & Nanny asset - TurboSquid](https://www.turbosquid.com/FullPreview/Index.cfm/ID/1028231)
* [How To Set Up Three Point Lighting In Unreal Engine 4.12](http://www.kembadesign.com/tutorials/2016/6/10/how-to-set-up-three-point-lighting-in-unreal-engine-412)

### Tasks Undertaken

1. Following steps 1 - 6 of the Lighting Quick Start Guide, set up the base scene for indoor lighting.
1. Expand the apartment somewhat by adding and moving walls so there is more space to work with, as well as creating more space for lighting to come inside, also added minimal furniture and a TV for added interest (screenshot from a show applied as an emmisive texture).
1. For the light-card based approach, we will add spot lights directly outside of each window, facing outwards, and place a plane close by facing into the room so it can bounce the light back inside. Give it a bright material that includes a multiplied colour value into the Emmisive Color channel.
    - This process can be somewhat sped up by creating one set with the spot light as a child to the plane, and then duplicating them to place by each other window.
    - For additional light, set the light-cards Cast Shadow value to false, and place an additional spot light behind it facing inwards along the same angle as the scene's directional light.
1. For the light portal based approach, simply position and size a lightmass portal into the frame of each window, and provided they are within the Lightmass Importance Volume, they will guide more light through the window openings.

### What we found out

Unreal Engine is renowned for the beautiful graphics that games made with it tend to have, and this is in big part due to the available lighting. Lighting in UE4 is based on how lighting behaves in the real world, notably in it's use of Global Illumination. Through use of GI, light paths coming from the scene can bounce many times, and they do so based on the particular properties of the materials that the hit such that all the objects interact as you would expect (for example, a red box sitting in a white room with a light directed towards it would have a subtle red glow touching the white ground just around the box). Much of this realistic lighting is computationally intensive, and as such is baked into the scenes through the process of light building.

Making lighting look good is very much an art in and of itself, and is a combination of technical adjustments and 'gut' feeling. Good lighting will vary depending on the type of scene one aims to reproduce, but in general the aim is to provide both good contrast, as well as a sense of life to the lights. There are a number of factors that will come together when doing this, but lighting strength and (often subtle) colour variations are of particular importance. In the example of a home illuminated during the day, one would expect to see a directional light of comparitively high intensity and a warm tint to represent the sun, as well as a sky light to add soft all-around light from the atmosphere. We can then also add extra lights to mimick additional light bounces that might not be bright enough with regular settings, as well as more standard indoor lighting that you would expect from actual lighting fixtures.

In UE4, lights are divided into four types. These are directional, point, spot and sky lights. Directional lights project light in a single direction as if coming from infinitely far away, and are useful as an approximation for the sun. Point lights emit light from a single point in space to all directions around them, and can be imagined as a simplified lightbulb. Spot lights act as one would expect, and project light in a limited cone much like a spot light in the real world might. Finally, sky lights act like a sky would, creating a more realistic lighting effect than a directional light alone might, by bringing light into a scene from all directions, based on the brightness of each point in the sky (which can be defined by a texture to create all sorts of lighting conditions).

In addition to these lighting types, they can also have three different Mobility types. These are static, stationary, and movable. Static lights are unable to move or change in any way during play, and as such have no overhead. Stationary lights are still unable to move, but gain the ability to change in brightness and colour at runtime (this creates some computational overhead). Finally, Movable lights are capable of changing all of their properties at runtime, and are the most computationally intensive of the three. They are however the most versatile.

Lightmass is Unreal Engine's way of baking parts of a game's lighting into the objects that populate a scene, so as to take a large chunk of the visually impressive yet computationally expensive lighting out of a game's runtime overhead. It only handles part of the lighting made by static and stationary lights, and is not compatible with movable lights. A Lightmass Importance Volume can be added to focus Lightmass' quality to areas where it is most important (i.e. places the player is expected to see particularly well). It holds a 3D grid of points which contain indirect lighting information from all directions as well as at a higher resolution atop surfaces facing upwards, which the player would be likely to walk on. This information is then used to interpolate indirect lighting for a Movable object in game.

In addition to simply pointing a directional light in through openings in an indoor space's walls, we can also use light-cards and lightmass portals to bring extra light in through things like windows where the light from a directional light alone might not be enough to brighten up a scene well. Using light-cards, a bright, reflective surface is placed close to a given window, with a wide spot light facing towards it, and the rays from this bounce from the surface back in through the window in a way that creates wide, soft lighting. The light-card can also maintain visibility to produce a bright, over-exposed effect on the window itself. As for lightmass portals, these are objects which you place in spaces like windows or hallway entrances where light might not travel well, and it effectively works in combination with the Lightmass Importance Volume to guide extra light through those openings. It doesn't emit light of its own, however, and requires there to be existing lighting outside the opening to be effective.

Box and Sphere Reflection Capture actors can be used to produce useful reflections in UE4. While the underlying mechanism remains somewhat mysterious, the general usage is that if you have reflective objects in an area and you would like them to reflect the things around them, you place either a Box or Sphere Reflection capture around them and the things you want to reflect, and it will provide the reflections that the reflective objects need. The added reflections also appear to be a way to brighten the area somewhat (see figure 1).

Three point lighting consists of an off-center strong light "Key light" which provides most of the lighting, a secondary light with diminished strength on the other side relative to the camera which fills in some shadow "Fill light" and a low strength light which sits behind the object and add a little bit of a glow around the edge of the object "Rim light". In Unreal Engine 4, we can replicate this using point and spot lights. One way to do it, described in `How To Set Up Three Point Lighting In Unreal Engine 4.12`, uses point lights for the Key and Rim lights, and a spot light for the Fill light. These lights will also vary somewhat in colour to make the resulting view less "flat". One choice would be to have a warmer Key light, and cooler Fill and Rim lights.

Lighting in Unreal Engine (and indeed most game engines) has always been something of a mystery for me in terms of making the lighting look good. It seems as though it will continue to be like this to some extent, as even following the `Lighting Quick Start Guide` exactly as described, I found my scene to be unexpectedly dark in comparison to what was shown in the tutorial (see figures 2 and 3). After setting things up mostly as they were in the tutorial, I went about adding light-cards and lightmass portals in each scene. Of the two, only the light-card in the bathroom seemed to make any noticeable difference, perhaps because of how dark it was to begin with. As there was only sky light based lighting coming in through the bathroom window I opted to add an additional spot light to help the lightmass portal have something to pass on, but it is unclear as to how much the portal actually assisted.

Part of why the effect might not have been too noticeable, is that a) my windows are fairly small to begin with, meaning any amount of light coming through is unlikely to make a huge difference, and b) two of the three windows in my scene are positioned on a wall adjacent to an almost entirely open wall, and while the windows are still separate to its direct lighting, the indirect lighting is likely to be quite bright, and may be obscuring some amount of the effect. I suspect both lightmass portals and light-cards work best in areas where there are no large illuminated surfaces nearby. Having the windows closer to the ground may have also had more effect, and in fact some brightness can be seen on the roof above each window, suggesting they are in fact bringing more lighting in.

Finally, adding the feature object proved to be something of a difficulty as the asset I found, when first imported, had half of it's normals facing the wrong way. I adjusted this within Blender before reimporting, but suspect there is likely a better way. Additionally, the asset came as an .obj with textures supplied separately, and I was unable to have these automatically come in together. This too was only a mild inconvenience though, as it meant I needed to click through each generated material to work out which part of the mesh it was responsible for, and apply textures to it as appropriate. I also opted to add a metallic material to the bunnies' eyes as a small extra detail.

### Open Issues/Risks

1. While the brief requests for a movable feature piece, I have used an Empty Project as my base, and this means that the deafult pawn used does not appear to affect anything in the world, even if it is marked as Movable and has collisions set up.
    * The solution is most likely just to have a proper Pawn setup added to the scene to allow interaction with the feature object. For now, the object's Mobility can be demonstrated by having it's starting location be somewhere unstable so that physics can move it for us.
1. The bathroom is still fairly dark in comparison to 

### Recommendations

It would be worth learning about UE4's behaviours in terms of both the automatic brightness adjustments the engine makes as you move through a scene, and the banding that can appear on surfaces with a flat colour as their material (see figure 4). Additionally, it would also be of use to learn further about reflection captures and how they work within a scene.

### Appendix

Figure 1: We can see the additional brightness gained by using a reflection capture.

![The reflection volume in action][Lighting04]

Figure 2: The bathroom is considerably darker than what the tutorial's screenshot suggested it would be.

![Unusually dark lighting][Lighting01]

Figure 3: The scene part-way through the process; the areas outside of the direct lighting are still quite dark (this is prior to increasing the number of indirect bounces).

![Scene in progress][Lighting02]

Figure 4: This shows both reflection capture based lighting differences, as well as a seam between two roof objects. This becomes less noticeable after expanding the reflection capture to cover the whole roof.

![Colour banding and seams between meshes][Lighting03]

[Lighting01]: https://monosnap.com/file/VlIFNVQExTlCQgWbv6ytACgzY2Qxuv.png "Unusually dark lighting"
[Lighting02]: https://monosnap.com/file/iFy5SCZoCVBfQFhPSxhQDCgvVV9RJj.png "Scene in progress"
[Lighting03]: https://monosnap.com/file/lb7sMGtFsw5ySD4j4kGzb25Idz4mK2.png "Colour banding and seams between meshes"
[Lighting04]: https://monosnap.com/file/rpp21xXMy2ASEetDjWudHFzWkDopbE.png "More obvious discolouration between different parts of the same mesh objects"
[Lighting05]: https://monosnap.com/file/uaXvPLCKZAm0pi7sDt9sLyNgSbAFWa.png "Full scene"
